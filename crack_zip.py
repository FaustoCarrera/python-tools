#!/usr/bin/env python
"""
Break the zip password
"""

import sys
import argparse
import zipfile
import shutil
import math
from itertools import product


class CrackZip(object):

    """
    This script will try to break the password of a zip file
    possible attacks:
    - bruteforce
    - dictionary
    - mask
    usage:
    python crack_zip.py --zip {zip file} --attack {b|d} --dict {/path/to/dictionary}
    python crack_zip.py --zip gpg.zip --attack m --len 10 --mask '#023CKRSabceklnorswz!'
    """

    def dictionary(self, zip_file, dict_file):
        "check zip password against dictionary"
        # zip file
        passwd = open(dict_file, 'r')
        z_file = zipfile.ZipFile(zip_file)
        dest_folder = './%s' % zip_file.replace('.zip', '')
        # flags
        pass_found = False
        pass_checked = 0
        # crack it
        for raw_pass in passwd:
            password = raw_pass.strip('\n')
            pass_checked += 1
            try:
                z_file.extractall(path=dest_folder, pwd=password)
                pass_found = True
                print 'password found: %s' % password
                break
            except Exception:
                pass
        print 'total pass checked %d' % pass_checked
        if not pass_found:
            print 'password not found on dictionary'

    def bruteforce(self, zip_file, pass_len, mask=None):
        "use brute force against zip file"
        z_file = zipfile.ZipFile(zip_file)
        dest_folder = './%s' % zip_file.replace('.zip', '')
        # chars
        if mask:
            chars_list = mask
        else:
            chars_list = '#!@$%^&*()_-+='
            chars_list += '0123456789'
            chars_list += 'abcdefghijklmnopqrstuvwxyz'
            chars_list += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        # flags
        count_generated = 0
        # generate passwords list
        if pass_len.isdigit():
            min_len = int(pass_len)
            max_len = min_len + 1
        else:
            (min_len, max_len) = pass_len.split('-')
        pass_base = []
        for i in range(int(min_len), int(max_len)):
            if i > 0 and i < 16:
                pass_base.append(i)
        # generate passwords
        for length in pass_base:
            print 'total passwords: %d' % math.pow(len(chars_list), i)
            to_attempt = product(chars_list, repeat=length)
            for attempt in to_attempt:
                password = ''.join(attempt)
                count_generated += 1
                # try to crack
                try:
                    z_file.extractall(path=dest_folder, pwd=password)
                    pass_found = True
                    print 'password found: %s' % password
                    break
                except Exception as excep:
                    pass
        print count_generated

    @staticmethod
    def export(filename, password):
        "send passwords to file"
        passwords_file = open(filename, 'a')
        passwords_file.write('%s\n' % password)
        passwords_file.close()


def arguments():
    "Parse cli arguments"
    parser = argparse.ArgumentParser(
        description='Zip files cracker using dictionary or bruteforce')
    parser.add_argument('--zip', required=True, type=str,
                        help='zip file', default=False)
    parser.add_argument('--attack', required=True, type=str,
                        help='type of attack', default='b', choices=['b', 'd', 'm'])
    parser.add_argument('--len', required=False, type=str,
                        help='password lenght [min-max]', default='8-9')
    parser.add_argument('--dict', required=False, type=str,
                        help='dictionary file if dictionary attack', default=False)
    parser.add_argument('--mask', required=False, type=str,
                        help='chars if mask attack', default=False)
    parsed_args = parser.parse_args()
    # check dictionary
    if parsed_args.attack == 'd' and not parsed_args.dict:
        parser.error('Dictionary file path is missing')
    # check mask
    if parsed_args.attack == 'm' and not parsed_args.mask:
        parser.error('Chars mask is missing')
    return parsed_args

if __name__ == '__main__':
    args = arguments()
    crack = CrackZip()
    if args.attack == 'd':
        crack.dictionary(args.zip, args.dict)
    if args.attack == 'b':
        crack.bruteforce(args.zip, args.len)
    if args.attack == 'm':
        crack.bruteforce(args.zip, args.len, args.mask)
