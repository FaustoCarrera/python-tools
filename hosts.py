#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Check for hosts
"""

import sys
import argparse
import json
from tabletext import to_text
try:
    import nmap
except ImportError as error:
    print error
    sys.exit(0)


def arguments():
    "Parse cli arguments"
    parser = argparse.ArgumentParser(
        prog=sys.argv[0], description='Script to scan ports')
    parser.add_argument(
        '--ip', required=True, type=str, help='ip address to scan', default=False)
    parser.add_argument('--format', required=False, type=str,
                        help='format', default='table', choices=['table', 'csv'])
    parsed_args = parser.parse_args()
    return parsed_args


def scan(ip, format):
    try:
        nm = nmap.PortScanner()
    except Exception as error:
        print error
        sys.exit(0)
    result = nm.scan(hosts=ip)
    # result
    if format == 'table':
        hosts = nm.all_hosts()
        table = []
        for host in hosts:
            result_h = result['scan'][host]
            status = result_h['status']['state']
            hostname = result_h['hostname']
            table.append(['host', 'hostname', 'status', '-'])
            table.append([host, hostname, status, ''])
            table.append(['host:port', 'product', 'name', 'status'])
            if 'tcp' in result_h:
                for port in result_h['tcp']:
                    product = result_h['tcp'][port]['product']
                    state = result_h['tcp'][port]['state']
                    name = result_h['tcp'][port]['name']
                    table.append(
                        ['%s:%s' % (host, port), product, name, state])
            table.append(['-' * 19, '-' * 19, '-' * 19, '-' * 6])
        print to_text(table)
    else:
        print nm.csv()


def main():
    "run scanner"
    args = arguments()
    scan(args.ip, args.format)

if __name__ == '__main__':
    main()
