#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
This script lists the exif data from PDF
"""

import argparse
import sys
import pyPdf
from pyPdf import PdfFileReader


def arguments():
    "Parse cli arguments"
    parser = argparse.ArgumentParser(
        prog=sys.argv[0], description='Script to list pdf exif data')
    parser.add_argument(
        '--pdf', required=True, type=str, help='PDF file', default=False)
    parsed_args = parser.parse_args()
    return parsed_args


def metadata(filename):
    pdf_file = file(filename, 'rb')
    pdf_file = PdfFileReader(pdf_file)
    doc_info = pdf_file.getDocumentInfo()
    print '=' * 50
    print 'reading metadata from %s' % filename
    for metaitem in doc_info:
        print '%s: %s' % (metaitem, doc_info[metaitem])
    print '=' * 50

if __name__ == '__main__':
    args = arguments()
    metadata(args.pdf)
