#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Sniff network packets
"""

import dpkt
import pcap
import socket


class Packets(object):

    interface = None

    def __init__(self, interface):
        self.interface = interface

    def sniff(self):
        pc = pcap.pcap(self.interface)
        # print 'listening on %s: %s' % (pc.name, pc.filter)
        for ts, buf in pc:
            # print ts, len(buf)
            try:
                eth = dpkt.ethernet.Ethernet(buf)
                ip = eth.data
                ip_src = socket.inet_ntoa(ip.src)
                ip_dst = socket.inet_ntoa(ip.dst)
                # local ip's
                area_network_src = 'internet'
                area_network_dst = 'internet'
                if '192.168.' in ip_src:
                    area_network_src = 'local'
                if '192.168.' in ip_dst:
                    area_network_dst = 'local'
                tcp = ip.data
                # result
                result = {
                    'src': {
                        'ip': ip_src,
                        'port': tcp.sport,
                        'area_network': area_network_src
                    },
                    'dst': {
                        'ip': ip_dst,
                        'port': tcp.dport,
                        'area_network': area_network_dst
                    },
                    'timestamp': ts
                }
                yield result
            except:
                pass

if __name__ == '__main__':
    # packets = Packets('eth0')
    packets = Packets('wlan0')
    for i in packets.sniff():
        print i
