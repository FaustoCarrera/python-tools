#!/usr/bin env python
# -*- coding: utf-8 -*-
"""
Read a tcpdump file, parse it and DISPLAY
"""

import re


class TcpDump(object):

    """
    Class to read a tcpdump file
    Create the tcpdump file with 'tcpdump -nNqt'
    """

    dumpfile = None

    def __init__(self, dumpfile):
        self.dumpfile = dumpfile

    def parse(self):
        "Parse tcpdump file"
        reg = re.compile(
            r"IP (?P<IP1>(?:\d{1,3}\.){3}\d{1,3})\.(?P<Port1>\d+) > (?P<IP2>(?:\d{1,3}\.){3}\d{1,3})\.(?P<Port2>\d+): (?:tc|ud)p (?P<size>\d+)")
        dump = open(self.dumpfile, 'r')
        for line in dump:
            line = line.strip('\n')
            try:
                m = reg.match(line)
                ip_src = m.group('IP1')
                ip_dst = m.group('IP2')
                # local ip's
                area_network_src = 'internet'
                area_network_dst = 'internet'
                if '192.168.' in ip_src:
                    area_network_src = 'local'
                if '192.168.' in ip_dst:
                    area_network_dst = 'local'
                # result
                result = {
                    'src': {
                        'ip': ip_src,
                        'port': m.group('Port1'),
                        'area_network': area_network_src
                    },
                    'dst': {
                        'ip': ip_dst,
                        'port': m.group('Port2'),
                        'area_network': area_network_dst
                    },
                    'length': m.group('size')
                }
                yield result
            except:
                pass
        dump.close()


def test():
    filename = './dumps/tcpdump'
    tcpdump = TcpDump(filename)
    for i in tcpdump.parse():
        print i

if __name__ == '__main__':
    test()
