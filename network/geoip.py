#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
GeoIP class
"""

import geoip2.database


class GeoIP(object):

    """
    GeoIP class
    Geolocate ip addresses using the GeoLite database
    http://dev.maxmind.com/geoip/geoip2/geolite2/
    You gonna need:
    - the MinMax database: http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.mmdb.gz
    - the MinMax DB lib: https://github.com/maxmind/libmaxminddb
    - the GeoIP 2 python lib: https://pypi.python.org/pypi/geoip2
    You gonna get a dictionary containing:
    - country iso code
    - country name
    - city name
    - postal code
    - latitude
    - longitude
    """

    reader = None

    def __init__(self):
        self.reader = geoip2.database.Reader('./database/GeoLite2-City.mmdb')

    def geolocate(self, ipaddress):
        "Geolocate an ip address and return data as dictionary"
        result = {'status': 'ok'}
        try:
            response = self.reader.city(ipaddress)
            result['country'] = str(response.country.iso_code)
            result['state'] = str(response.subdivisions[0].name)
            result['city'] = str(response.city.name)
            result['zip'] = str(response.postal.code)
            result['timezone'] = str(response.location.time_zone)
            result['lat'] = float(response.location.latitude)
            result['lon'] = float(response.location.longitude)
        except ValueError as error:
            result['status'] = 'error'
            result['message'] = error[0]
        except geoip2.errors.AddressNotFoundError as error:
            result['status'] = 'error'
            result['message'] = error[0]
        return result

    def georaw(self, ipaddress):
        "Geolocate an ip address and return data as raw object"
        try:
            response = self.reader.city(ipaddress)
            print response
            return response
        except ValueError as error:
            return error[0]

    def close(self):
        "Close database connection"
        self.reader.close()


def test():
    "Geoip basic test"
    geoip = GeoIP()
    print geoip.geolocate('69.64.71.152')
    print geoip.georaw('69.64.71.152')
    print geoip.geolocate('192.168.0.100')
    geoip.close()

if __name__ == '__main__':
    test()
