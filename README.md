#Python tools

Different type of python tools

##Tools

* crack passwords
* crack zip files
* hosts discovery
* ports scanner
* PDF exif data
* ssh bruteforce
* geolocate ip
* read packets
* read tcpdump file

##Install

* pip install python-nmap
* pip install pyPdf
* pip install mechanize
* pip install BeautifulSoup4
* apt-get install python-bluez
* apt-get install bluetooth
* apt-get install python-obexftp
* pip install pexpect

##Pcap
* sudo apt-get install libpcap-dev
* sudo apt-get install python-pypcap
* sudo -H pip install dpkt
* [dpkt Tutorial #2: Parsing a PCAP File](https://jon.oberheide.org/blog/2008/10/15/dpkt-tutorial-2-parsing-a-pcap-file/)

##GeoIP

* [GeoLite 2](http://dev.maxmind.com/geoip/geoip2/geolite2/)
* [GeoLite2 City MaxMin DB](http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.mmdb.gz)
* [Lib MaxMin DB](https://github.com/maxmind/libmaxminddb)
* [Python GeoIP 2](https://pypi.python.org/pypi/geoip2)
