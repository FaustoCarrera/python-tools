#!/usr/bin/env python
"""
Test passwords using different hash algos:
crypt, MD5, SHA1, SHA224, SHA256, SHA384, SHA512
Constructors present in this module are:
crypt, md5(), sha1(), sha224(), sha256(), sha384(), and sha512()
"""

import argparse
import crypt
import hashlib
from threading import Thread, Semaphore


class DecryptPasswords(object):

    "Class to decrypt passwords"

    algorythm = None
    passwords = None
    dictionary = None

    def __init__(self, algorythm, passwords, dictionary):
        self.algorythm = algorythm
        self.passwords = passwords
        self.dictionary = dictionary

    def run(self):
        "run decryptor"
        dictionary_list = self.get_contents(self.dictionary)
        passwords_list = self.get_contents(self.passwords)
        decryptor = Decrypt(self.algorythm)
        count = 1
        for password in passwords_list:
            thread = Thread(
                target=decryptor.run,
                args=(password, dictionary_list, count)
            )
            thread.start()
            count += 1

    @staticmethod
    def get_contents(filename):
        "read contents from file"
        contents_list = []
        contents_raw = open(filename, 'r')
        for content in contents_raw:
            content = content.replace('\r', '')
            content = content.strip('\n')
            contents_list.append(content)
        return contents_list


class Decrypt(object):

    "Class to decrypt passwords"

    algorythm = None
    screenlock = None

    def __init__(self, algorythm):
        self.algorythm = algorythm
        self.screenlock = Semaphore(value=1)

    def run(self, password, dictionary_list, count):
        "decrypt password"
        cracked = self.crackit(password, dictionary_list)
        self.screenlock.acquire()
        if cracked:
            print count, cracked
        else:
            print count, '-'
        self.screenlock.release()

    def crackit(self, password, dictionary_list):
        "try to crack the password"
        for word in dictionary_list:
            if self.algorythm == 'crypt':
                cracked = self.hash_crypt(word, password)
            if self.algorythm == 'sha1':
                cracked = self.hash_sha1(word, password)
            if self.algorythm == 'sha224':
                cracked = self.hash_sha224(word, password)
            if self.algorythm == 'sha256':
                cracked = self.hash_sha256(word, password)
            if self.algorythm == 'sha384':
                cracked = self.hash_sha384(word, password)
            if self.algorythm == 'sha512':
                cracked = self.hash_sha512(word, password)
            if cracked:
                return cracked
        return False

    @staticmethod
    def hash_crypt(clear_pass, encrypted_pass):
        "Linux crypt"
        pass_hash = crypt.crypt(clear_pass, encrypted_pass)
        if pass_hash == encrypted_pass:
            return '%s' % clear_pass
        return False

    @staticmethod
    def hash_sha1(clear_pass, encrypted_pass):
        "SHA1"
        if hashlib.sha1(clear_pass).hexdigest() == encrypted_pass:
            return '%s' % clear_pass
        return False

    @staticmethod
    def hash_sha224(clear_pass, encrypted_pass):
        "SHA224"
        if hashlib.sha224(clear_pass).hexdigest() == encrypted_pass:
            return '%s' % clear_pass
        return False

    @staticmethod
    def hash_sha256(clear_pass, encrypted_pass):
        "SHA256"
        if hashlib.sha256(clear_pass).hexdigest() == encrypted_pass:
            return '%s' % clear_pass
        return False

    @staticmethod
    def hash_sha384(clear_pass, encrypted_pass):
        "SHA384"
        if hashlib.sha384(clear_pass).hexdigest() == encrypted_pass:
            return '%s' % clear_pass
        return False

    @staticmethod
    def hash_sha512(clear_pass, encrypted_pass):
        "SHA512"
        if hashlib.sha512(clear_pass).hexdigest() == encrypted_pass:
            return '%s' % clear_pass
        return False


def arguments():
    "Parse cli arguments"
    parser = argparse.ArgumentParser(description='Dictionary password cracker')
    parser.add_argument('--a', required=True, type=str,
                        help='algorythm to use',
                        choices=[
                            'crypt',
                            'md5',
                            'sha1',
                            'sha224',
                            'sha256',
                            'sha384',
                            'sha512'
                        ])

    parser.add_argument('--f', required=True, type=str,
                        help='passwords file', default=False)

    parser.add_argument('--d', required=True, type=str,
                        help='dictionary file', default=False)

    parsed_args = parser.parse_args()
    return parsed_args


def main():
    "start decripting passwords passwords"
    algos = ['crypt', 'md5', 'sha1', 'sha224', 'sha256', 'sha384', 'sha512']
    args = arguments()
    if args.a in algos:
        passwords = DecryptPasswords(args.a, args.f, args.d)
        passwords.run()


if __name__ == '__main__':
    main()
