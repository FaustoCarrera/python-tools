#!/usr/bin/env python
"""
Check open ports
"""

import os
import sys
import argparse
import nmap
from socket import *
from threading import Thread


class Scanner(object):

    "class to scan ports"

    ipaddress = None
    ports_file = None
    scanner = None

    def __init__(self, ipaddress, ports_file, scanner):
        self.ipaddress = ipaddress
        self.ports_file = ports_file
        self.scanner = scanner

    def scan(self):
        "start scanner"
        results = []
        # get the ports list
        ports = self.get_ports(self.ports_file)
        # setup scanner
        if self.scanner == 'socket':
            print 'ip|port|result'
        else:
            print 'ip|port|hostname|status|state|reason'
        # parse ports
        for port in ports:
            if self.scanner == 'socket':
                thr = SocketTester(self.ipaddress, port)
            else:
                thr = NmapTester(self.ipaddress, port)
            results.append(thr)
            thr.setDaemon(True)
            thr.start()
        # print results
        for result in results:
            result.join()
            print result.status

    @staticmethod
    def get_ports(filename):
        "read ports list"
        filter_list = {}
        contents_list = []
        contents_raw = open(filename, 'r')
        for content in contents_raw:
            content = content.replace('\r', '')
            content = content.strip('\n')
            content = content.split(';')
            port_data = (content[0], content[3])
            filter_list[content[0]] = port_data
        # filter list
        for key in sorted(filter_list.keys()):
            contents_list.append(filter_list[key])
        return contents_list


class SocketTester(Thread):

    "Test port using sockets"
    ipaddress = None
    port = None
    status = None

    def __init__(self, ipaddress, port):
        Thread.__init__(self)
        self.ipaddress = ipaddress
        self.port = port

    def run(self):
        "test port"
        try:
            socket_conn = socket(AF_INET, SOCK_STREAM)
            socket_conn.settimeout(10)
            socket_conn.connect((self.ipaddress, int(self.port[0])))
            socket_conn.send('Hello cthulu\r\n')
            result = socket_conn.recv(100)
            socket_conn.close()
            result = str(result)[0:80]
            result = result.replace('\r', '')
            result = result.replace('\n', '')
            self.status = '%s|%s|%s' % (
                self.ipaddress,
                self.port[0],
                result
            )
        except:
            self.status = '%s|%s|%s' % (self.ipaddress, self.port[0], None)


class NmapTester(Thread):

    "test port using Nmap"
    ipaddress = None
    port = None
    status = None

    def __init__(self, ipaddress, port):
        Thread.__init__(self)
        self.ipaddress = ipaddress
        self.port = port

    def run(self):
        "test port"
        try:
            nm_scan = nmap.PortScanner()
            result = nm_scan.scan(self.ipaddress, self.port[0])
            self.status = '%s|%s|%s|%s|%s|%s' % (
                self.ipaddress,
                self.port[0],
                result['scan'][self.ipaddress]['hostname'],
                result['scan'][self.ipaddress]['status']['reason'],
                result['scan'][self.ipaddress]['status']['state'],
                result['scan'][self.ipaddress]['tcp'][
                    int(self.port[0])]['reason'],
            )
        except:
            self.status = '%s|%s|%s|%s|%s|%s' % (
                self.ipaddress, self.port[0], None, None, None, None)


def arguments():
    "Parse cli arguments"
    parser = argparse.ArgumentParser(
        prog=sys.argv[0], description='Script to scan ports')
    parser.add_argument('--ip', required=True, type=str,
                        help='ip address to scan', default=False)
    parser.add_argument('--ports', required=True, type=str,
                        help='ports csv list', default=False)
    parser.add_argument('--scanner', required=False, type=str,
                        help='what scanner to use', choices=['nmap', 'socket'], default='socket')
    parsed_args = parser.parse_args()
    return parsed_args


def main():
    "run scanner"
    args = arguments()
    if args.ip and args.ports:
        scanner = Scanner(args.ip, args.ports, args.scanner)
        scanner.scan()

if __name__ == '__main__':
    main()
